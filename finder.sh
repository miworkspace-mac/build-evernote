#!/bin/bash

#NEWLOC=`curl "https://evernote.com/download/get.php?file=EvernoteMac" 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .dmg | tail -1 | sed 's@\.\/@@'`
#NEWLOC=$(curl -L https://evernote.com/download/ 2>/dev/null | grep .zip | awk -F'url:' '{print $3}' | awk -F' ' '{print $1}' | sed 's/\\n$//')
#NEWLOC=`curl -L "https://evernote.com/download/" 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .dmg | tail -1 | sed 's@\.\/@@'`

NEWLOC="https://mac.desktop.evernote.com/builds/Evernote-latest.dmg"


if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi