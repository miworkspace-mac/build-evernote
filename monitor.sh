#!/bin/bash -ex

#download dmg
#NEWLOC=`curl -L "https://evernote.com/download/" 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .dmg | tail -1 | sed 's@\.\/@@'`

NEWLOC="https://mac.desktop.evernote.com/builds/Evernote-latest.dmg"


curl -L -o Evernote.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${NEWLOC}"

#mount downloaded DMG
mountpoint=`yes | hdiutil attach -mountrandom /tmp -nobrowse Evernote.dmg | awk '/private\/tmp/ { print $3 } '`

#locate app within downloaded DMG
app_in_dmg=$(ls -d $mountpoint/*.app)

# Obtain version info from APP
VERSION=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" $app_in_dmg/Contents/Info.plist`

if [ "x${VERSION}" != "x" ]; then
	echo "${VERSION}"
fi

hdiutil detach "${mountpoint}"

rm -rf Evernote.dmg


if [ "x${VERSION}" != "x" ]; then
    echo version: "${VERSION}"
    echo "${VERSION}" > current-version
fi

# Update to handle distributed builds
if cmp current-version old-version; then
    # Files are identical, exit 1 to NOT trigger the build job
    exit 1
else
    # Files are different - copy marker, exit 0 to trigger build job
    cp current-version old-version
    exit 0
fi