#!/bin/bash -ex

# CONFIG
prefix="Evernote"
suffix=""
munki_package_name="Evernote"
display_name="Evernote"
icon_name=""
description="This update contains stability and security fixes for Evernote."
category="Productivity"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
#mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack /usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg
# mkdir build-root
# (cd build-root; pax -rz -f ../pkg/*/Payload)
# hdiutil detach "${mountpoint}"

## Find all the appropriate apps, etc, and then turn that into -f's
# key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
# echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
# perl -p -i -e 's/build-root//' app.plist
## END EXAMPLE

# Build pkginfo
/usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg > app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# For silent packages, configure blocking_applications and unattended_install
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.10.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" description -string "${description}"

# Obtain update description from MacUpdate and add to plist
# description=`/usr/local/bin/mutool --update 27456` #(update to corresponding MacUpdate number)
# defaults write "${plist}" description -string "${description}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
